# Cloud Formation Wrapper

This library contains some helper scripts to make AWS CloudFormation more usable, as well as provide a framework for how to define stacks consistently. 

This should make stacks simpler to maintain over time. 

There are also some demonstration layouts that can be used as a starting point. These can be found under [base-layouts](base-layouts).


## Stack Structure
Nested Stacks are used to keep the templates a manageble size and increase re-use for common components.

The stack is divided into 3 logical areas:

- services (organisations, actionable, etc.)
- resources (e.g. databases, swf)
- network (e.g. vpc, NAT)

Each folder also has a matching template name that is the parent stack, i.e `services.yml` is the template 
that calls all the templates in `services/` such as `services/cms.yml`

Additionally, common templates are in the _common_ folder.

### Requirements
The base of the stack must be in the root folder and named `stack.yml`. It must take the base parameters specified below.

### Parameters
The build script automatically provides the following parameters to the stack. Additional parameters can be specified, 
see _Custom Parameters_.

| Parameters             | Description                                                                                       |
| ---------------------- | ------------------------------------------------------------------------------------------------- |
| CloudFormationStackUrl | The base url that the stacks will be uploaded too (in the same file structure as the current dir) |
| DeploymentEnvironment  | The environment being deployed (one of staging, develop, test, production, feature)               |
| FeatureName            | Feature name for feature staging. Required when _DeploymentEnvironment_ is `feature`              |

#### Custom Parameters
Custom parameters are defined using JSON in a file named `build-parameters.json`. They are in the AWS create-stack Parameter
format, described below.

JSON Syntax:
```javascript
[
  {
    "ParameterKey": "string",
    "ParameterValue": "string",
    "UsePreviousValue": true|false
  }
]
````


## Usage
All updates to the stacks are done the the `./build.sh` script.

The command line options are
  `./build.sh [staging|develop|test|production|feature] [options]...?`
  
For full information on running the build script, run `./build.sh --help`
  
  
### Options

| Paramenter                  | Description                                                                               |
| --------------------------- | ----------------------------------------------------------------------------------------- |
| `--dry-run`                 | Don't send changes to CloudFormation, just preprocess and show settings.                  |
| `--update-lambda-functions` | Build and upload lambda functions. This will still happen even with the `--dry-run` flag. |
| `--help`                    | Print the help information.                                                               |
| `--create`                  | Create a new stack.                                                                       |
| `--feature-name=`           | Feature name for feature staging. Required when _environment_ is `feature`.               |
| `--region=`                 | Region to deploy to. Uses AWS configuration or the `AWS_DEFUALT_REGION` env setting by default |

#### Example Usage
Please ensure that your profile has the privileges required to create roles and VPCs.

##### Create New Stack
This will run the 'pre-build' stack; execute this command again to create the main stack.
`./build.sh staging --create`

##### Update Development Stack
`./build.sh develop`

#### Check your settings but don't do anything
`./build.sh production --dry-run`

#### Stage a feature branch
`./build.sh feature --create --feature-name=MyFabulousFeature`


### Pre-Processing YAML Templates
The build script uses a template compiler called `cfn-include`.

Basically it adds the function `!Include` to the template which can be used to bring other yaml files in.
The standard I have used for this is that templates for compiling have the file suffix `.yml.template` and
files that are intended to be used as includes have the suffix `.yml.include`. The pre-processor then makes
a `.yml` file with the same name as the template.

All `yml.template` files will be automatically complied when running the build script.

For usage information, see https://www.npmjs.com/package/cfn-include

## Resources
### Lambda Functions
Lambda Functions are a little more complex than most of the resources controlled. The functions are defined 
in the `/lambdas` folder as node packages, with the folder name being the name the function will be called.

The functions are built and deployed to S3, the function's source code will come from there. The build script
will ensure the stack always has the latest version of the source code in it.

All lambda functions are described in the `resources/lambda.yml` stack.

#### Updating or Creating Lambda Functions
If the code of the lambda function has changed, there's a new function or the libraries need to be updated then
the build script can take care of this. You need to run with the `--update-lambda-functions` flag.

This will run `npm install && npm run zip` in each lambda and the copy the files to S3.

#### Getting the Lambda Source Data
Every time the build script runs it gets information about the Lambda Function source code from the deployment
bucket in S3. This is required to ensure the stack uses the latest object ID (which is how it knows to update
the function).

It then creates a mapping `yml.include` function that is available in the `resources/lambda/lambda-function.yml` 
stack.

### Route53 (DNS)
#### Subdomains
Due to a limitation with CloudFormation, when you create a new _subdomain_ as a hosted zone (e.g. 
development.conversations.actionable.co), the required NS record is not added to the parent domain. A script has
been added called `register-subdomain.sh` which will do this for you. It should be run after cloudformation is
finished if you are creating or updating the hosted zone itself (not recordsets). The script is idempotent - 
running it multiple times will not cause issues. 

## Some things that need to be made manually still
 - original hostedzone
 - Origin Access ID for cloudfront to use to connect to S3 (Goto )
 - SNS platform application
 - keypairs
 - deployment bucket (must have the name of the (lowercase) `stack description`_-deployment_ and be in the same region as the stack.)