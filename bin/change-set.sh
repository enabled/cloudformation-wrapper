#!/usr/bin/env bash

# Usage ./update.sh [staging|develop|production]
CONFIG_PATH="CloudFormation"
CONFIG_URL="https://s3-ap-southeast-2.amazonaws.com/deployment/$CONFIG_PATH/"

if [ -z "$1" ]; then
  echo "Must provide environment name. E.g. ./build.sh develop"
  exit 1
fi

if [ -z "$2" ]; then
  echo "Must provide changeset name. E.g. ./build.sh develop my-latest-change"
  exit 1
fi

GIT_COMMIT=$3
if [ -z "$3" ]
then
	echo "Git Commit not provided, Setting to current commit."
    GIT_COMMIT=`git rev-parse HEAD`
fi

DEPLOYMENT_ENV=$1
CHANGE_SET=$2
PRODUCTION=production
STACK_NAME="Conversations-$(tr '[:lower:]' '[:upper:]' <<< ${DEPLOYMENT_ENV:0:1})${DEPLOYMENT_ENV:1}"

REGION="ap-southeast-2" && [[ $DEPLOYMENT_ENV == $PRODUCTION ]]  && REGION="eu-west-1"

echo "Upload changed config files..."
aws s3 sync ./ s3://deployment/$CONFIG_PATH/ --exclude "*" --include "*.yml" || exit 1

echo "Creating change set $CHANGE_SET for $STACK_NAME in $REGION..."
aws cloudformation create-change-set --region $REGION --stack-name $STACK_NAME  \
  --change-set-name $CHANGE_SET  \
  --template-url ${CONFIG_URL}stack.yml \
  --capabilities CAPABILITY_NAMED_IAM \
  --parameters \
    ParameterKey=CloudFormationStackUrl,ParameterValue=$CONFIG_URL    \
    ParameterKey=DeploymentEnvironment,ParameterValue=$DEPLOYMENT_ENV \ 
    ParameterKey=ImageSuffix,ParameterValue=$GIT_COMMIT 

