#!/usr/bin/env bash

#
# Simple script to take a layout from the base-layouts folder and initialise it with a new name 
#

NAME_INPUT=$1
LAYOUT_INPUT=$2
REPLACE_INPUT=$3

PROJECT_NAME=$NAME_INPUT
LAYOUT_NAME=${LAYOUT_INPUT:-default}
REPLACE_NAME=${REPLACE_INPUT:-ReplaceMe}



##### Set some stuff up
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
reset=`tput sgr0`
bold=`tput bold`

W=$(echo -e "\xE2\x9A\xA0") # Exclamation
O=$(echo -e "\xE2\x9C\x93") # Check
X=$(echo -e "\xE2\x9D\x8C") # Cross

OK="${green}${O} OK${reset}"
FAILED="${red}${X} Failed${reset}"
SKIPPED="${yellow}${W} Skipped${reset}"

MISSING_DEPENDENCY="${red}${W} Missing Dependency${reset}"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../base-layouts" && pwd )"
ERROR_UNKNOWN=1
#################

error_exit() {
  ERROR_CODE=${1:-$ERROR_UNKNOWN}
  ERROR_MESSAGE=${2:-$FAILED}
  echo $ERROR_MESSAGE
  exit $ERROR_CODE
}

print_help() {
  echo 
  echo "This script copies one of the template layouts into a new project and changes the names."
  echo
  echo 
  echo "${bold}Usage${reset}"
  echo
  echo "      ./create-stack-from-layout.sh [project-name] [layout-name]?"
  echo
  echo "  ${bold}project-name${reset}"
  echo "  Required: yes"
  echo "  Description: The new project name to use in descriptions. For example 'Medelink' or 'GTFP'."
  echo
  echo
  echo "  ${bold}layout-name${reset}"
  echo "  Required: no"
  echo "  Default: default"
  echo "  Description: The template name in base-layouts."
  echo
  exit ${1:-0}
}

# -----------------------------------------------------------------------------
echo "${bold}Creating a new stack from template${reset}"
# -----------------------------------------------------------------------------
echo -n "    copy $LAYOUT_NAME files "
rsync -Pavq $DIR/$LAYOUT_NAME/ ./ || error_exit
echo $OK

echo -n "    update project name to $PROJECT_NAME files "
find . -type f -name '*.yml' -print0 -exec sed -i '' "s/${REPLACE_NAME}/${PROJECT_NAME}/g" {} \; > /dev/null  || error_exit
echo $OK

LC_PROJECTNAME=$(echo $PROJECT_NAME | tr '[:upper:]' '[:lower:]')
LC_REPLACE_NAME=$(echo $REPLACE_NAME | tr '[:upper:]' '[:lower:]')
echo -n "    update object references to $LC_PROJECTNAME"
find . -type f -name '*.yml' -print0 -exec sed -i '' "s/${LC_REPLACE_NAME}/${LC_PROJECTNAME}/g" {} \; > /dev/null || error_exit
echo $OK

echo "Done."