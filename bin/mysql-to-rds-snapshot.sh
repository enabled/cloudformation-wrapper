#!/usr/bin/env bash

# We need an easy way to create an RDS snapshot from a mysql file, so we can create

red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
reset=`tput sgr0`
bold=`tput bold`

W=$(echo -e "\xE2\x9A\xA0") # Exclamation
O=$(echo -e "\xE2\x9C\x93") # Check
X=$(echo -e "\xE2\x9D\x8C") # Cross

MISSING_DEPENDENCY="${red}${W} Missing Dependency${reset}"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# -- Check dependencies --
command -v jq > /dev/null 2>&1 || { echo >&2 "${MISSING_DEPENDENCY} Building requires 'jq', install with 'brew install jq'"; exit 1; }

print_help() {
  echo
  echo "mysql-to-rds-snapshot.sh is used to create or update the TFT stack for a single environment."
  echo
  echo "${bold}Usage${reset}"
  echo
  echo "      ./mysql-to-rds-snapshot.sh [options]?"
  echo
  echo "  ${bold}Options${reset}"
  echo "  -m, --mysql=PATH_TO_FILE"
  echo "    Path to mysql file to use in snapshot"
  echo
  echo "  -s, --snapshot-name=SNAPSHOT_NAME"
  echo "    Name of snapshot"
  echo "    NOTE: The parameter DBSnapshotIdentifier is not a valid identifier. Identifiers must begin with a letter; must contain only "
  echo "          ASCII letters, digits, and hyphens; and must not end with a hyphen or contain two consecutive hyphens."
  echo
  echo "  -d, --db-name=DB_NAME"
  echo "    Name of database"
  echo
  echo "  -h, --help, help"
  echo "    Print out the help (this text) and exit."
  echo
  exit ${1:-0}
}

DB_INSTANCE_IDENTIFIER='mysql-snapshot-instance'

# -- Check flags --
for i in "$@"
do
case $i in
    -h|--help|help)
    print_help
    shift
    ;;
    -s=*|--snapshot-name=*)
    SNAPSHOT_NAME="${i#*=}"
    shift
    ;;
    -m=*|--mysql=*)
    PATH_TO_MYSQL_DUMP="${i#*=}"
    shift
    ;;
    -d=*|--db-name=*)
    DB_NAME="${i#*=}"
    shift
    ;;
    *)
        error_exit $ERROR_INVALID_OPTIONS "${red}Unknown option ${bold}${i}${reset}${red} provided. Use --help for usage instructions.${reset}"
    ;;
esac
done


# -- Check Required Inputs --
if [ -z "$SNAPSHOT_NAME" ]; then
  echo "${red}${W} Missing Argument.${reset} Must provide snapshot name. E.g. ${bold}./mysql-to-rds-snapshot.sh -s=SNAPSHOT_NAME${reset}"
  print_help $ERROR_INCOMPLETE
fi

if [ -z "$PATH_TO_MYSQL_DUMP" ]; then
  echo "${red}${W} Missing Argument.${reset} Must provide path to mysql dump file. E.g. ${bold}./mysql-to-rds-snapshot.sh --mysql=/path/to/file.sql${reset}"
  print_help $ERROR_INCOMPLETE
fi

if [ -z "$DB_NAME" ]; then
  echo "${red}${W} Missing Argument.${reset} Must provide path to mysql dump file. E.g. ${bold}./mysql-to-rds-snapshot.sh --db-name=database_name${reset}"
  print_help $ERROR_INCOMPLETE
fi

if [ ! -f "$PATH_TO_MYSQL_DUMP" ]; then
  echo "${red}${W} Cannot find dump${reset} File $PATH_TO_MYSQL_DUMP does not exist."
  exit 2
fi

#TODO -- Check Snapshot doesnt already exist --

aws cloudformation deploy --stack-name MysqlSnapshotStack --template-file $DIR/mysql-to-rds-shapshot-stack.yml \
  --parameter-overrides DBName=$DB_NAME || exit 1

RDS_URL=$(aws cloudformation list-exports | jq --raw-output '.Exports [] | select(.Name == "rds-snapshot-instance-host") | .Value')

echo -n "Uploading mysql file to RDS Instance..."
mysql -h $RDS_URL -u root -pmysql-snapshot-instance-password $DB_NAME < $PATH_TO_MYSQL_DUMP || exit 1
echo "Done"

echo -n "Creating DB Snapshot..."
aws rds create-db-snapshot --db-instance-identifier $DB_INSTANCE_IDENTIFIER --db-snapshot-identifier $SNAPSHOT_NAME > /dev/null
echo "Done"

echo -n "Waiting for snapshot to complete..."
aws rds wait db-snapshot-completed --db-snapshot-identifier $SNAPSHOT_NAME
echo "Done"

aws cloudformation delete-stack --stack-name MysqlSnapshotStack

aws cloudformation wait stack-delete-complete --stack-name MysqlSnapshotStack

