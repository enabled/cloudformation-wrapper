#!/usr/bin/env bash

# CloudFormation can't create the NS records for a subdomain, this will create it.
# This needs to be run only after changing the zones in cloudfront

# -- Check Required Inputs --
if [ -z "$1" ]; then
  echo "Must provide subdomain name. E.g. ./register-subdomain.sh develop.enabled.com.au"
  echo "The AWS hosted zone for both the subdomain and it's parent must exist."
  exit 1
fi

command -v jq >/dev/null 2>&1 || { echo >&2 "register-subdomain requires 'jq', install with 'brew install jq'"; exit 1; }

SUBDOMAIN=$1
DOMAIN=${SUBDOMAIN#*.}.

if [ "$SUBDOMAIN" == "--help" ] || [ "$SUBDOMAIN" == "help" ]; then
  echo "CloudFormation can't create the NS records for a subdomain, this will create it."
  echo "This needs to be run only after changing the zones in cloudfront"
  echo
  echo "Usage"
  echo "  register-domain.sh [DOMAIN]"
  echo
  echo "Parameters"
  echo "  DOMAIN"
  echo "    Required: yes"
  echo "    The domain name to register as a subdomain. For instance, DOMAIN=my.domain.com.au would create an NS record"
  echo "    for my.domain.com.au in the domain.com.au zone."
  echo
  exit 1
fi

echo "Finding hosted zone for subdomain $SUBDOMAIN."
SUBDOMAIN_ID=`aws route53  list-hosted-zones \
  | jq --arg domain $SUBDOMAIN. '.HostedZones | map( select( .Name == $domain) ) |.[0].Id' \
  | sed -e 's/^"//' -e 's/"$//'`
echo "  $SUBDOMAIN_ID"

echo "Finding hosted zone for domain $DOMAIN"
DOMAIN_ID=`aws route53  list-hosted-zones \
  | jq --arg domain $DOMAIN '.HostedZones | map( select( .Name == $domain) ) |.[0].Id' \
  | sed -e 's/^"//' -e 's/"$//'`
echo "  $DOMAIN_ID"

echo "Get NS record for subdomain hosted zone $SUBDOMAIN_ID and generate changeset"
aws route53 get-hosted-zone --id $SUBDOMAIN_ID \
   | jq '.HostedZone.Name as $dn | .DelegationSet.NameServers | map({"Value": .}) | {Changes: [{Action: "UPSERT", ResourceRecordSet: {Name: $dn, Type: "NS", TTL: 600, ResourceRecords: .}}]}' \
   > changeset.json || exit 1

echo "Creating or updating NS record set in parent domain zone $DOMAIN_ID"
aws route53 change-resource-record-sets --hosted-zone-id $DOMAIN_ID --change-batch file://changeset.json || exit 1

rm changeset.json