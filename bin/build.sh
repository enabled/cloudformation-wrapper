#!/usr/bin/env bash
# 
# Build script for cloudformation-wrapper.
#


# ---- Setup some nice output variables -----
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
reset=`tput sgr0`
bold=`tput bold`

W=$(echo -e "\xE2\x9A\xA0") # Exclamation
O=$(echo -e "\xE2\x9C\x93") # Check
X=$(echo -e "\xE2\x9D\x8C") # Cross

OK="${green}${O} OK${reset}"
FAILED="${red}${X} Failed${reset}"
SKIPPED="${yellow}${W} Skipped${reset}"

# ---- Define Exit Error Codes ----
ERROR_UNKNOWN=1
ERROR_INVALID_TEMPLATE=20
ERROR_NPM=30
ERROR_S3=40
ERROR_INCOMPLETE=50
ERROR_AWS_CLI=60
ERROR_INVALID_OPTIONS=70
ERROR_INVALID_JSON=80

error_exit() {
  ERROR_CODE=${1:-$ERROR_UNKNOWN}
  ERROR_MESSAGE=${2:-$FAILED}
  echo $ERROR_MESSAGE
  exit $ERROR_CODE
}

# ---- Check dependencies ----
MISSING_DEPENDENCY="${red}${W} Missing Dependency${reset}"
command -v jq > /dev/null 2>&1 || { echo >&2 "${MISSING_DEPENDENCY} Building requires 'jq', install with 'brew install jq'"; exit 1; }
command -v cfn-include >/dev/null 2>&1 || { echo >&2 "${MISSING_DEPENDENCY} Building requires 'cfn-include', install with npm install --global cfn-include"; exit 1; }
command -v json2yaml >/dev/null 2>&1 || { echo >&2 "${MISSING_DEPENDENCY} Building requires 'json2yaml', install with npm install --global json2yaml"; exit 1; }


# ---- Define Help Documentation ----
print_help() {
  echo 
  echo "cf-build 1.0.1"
  echo
  echo "build.sh is used to create or update the TFT stack for a single environment."
  echo
  echo "It assumes at least one CloudFormation stack file is defined in the current directory"
  echo "named stack.yml."
  echo 
  echo "${bold}Usage${reset}"
  echo
  echo "      ./build.sh [environment] [options]?"
  echo
  echo "  ${bold}Environment${reset}"
  echo "  Required: yes"
  echo "  Possible Values: develop|staging|production|feature"
  echo "  Description: The environment to be deployed"
  echo  
  echo "  ${bold}Options${reset}"
  echo "  --create, -c"
  echo "    Create stack instead of update."
  echo  
  echo "  --skip-ecr"
  echo "    Use this to skip checking ecr repository creation"
  echo  
  echo "  -i, --image-suffix"
  echo "    Use this to add a suffix to the Docker image tag, Defaults to 'latest'."
  echo
  echo "  -l, --update-lambda-functions"
  echo "    Use this if updates have been made to the lambda functions that need to be built and deployed. Will"
  echo "    run 'npm install' in each function and upload a new version to S3."
  echo
  echo "  --dry-run, -d"
  echo "    Don't update the stack, print out the options. Note this will still update the lambda functions if told to do so."
  echo
  echo "  -h, --help, help"
  echo "    Print out the help (this text) and exit."
  echo
  echo "  -f, --feature-name=FEATURE_NAME"
  echo "    Special feature staging option for deploying a once-off version of the architecture. Must be used with environment"
  echo "    set to feature"
  echo
  echo "  --region=AWS_REGION"
  echo "    Set the region to deploy to. Defaults to AWS_DEFAULT_REGION, then the output of 'aws configure get region'."
  echo
  exit ${1:-0}
}


# ---- Get Script Arguments ----
PRODUCTION=production
CREATE_OR_UPDATE="update"
AWS_CONFIGURED_REGION=`aws configure get region`
REGION=${AWS_DEFAULT_REGION:=$AWS_CONFIGURED_REGION}
IMAGE_SUFFIX="latest"

for i in "$@"
do
case $i in
    -l|--update-lambda-functions)
    UPDATE_LAMBDAS=yes
    shift 
    ;;
    -d|--dry-run)
    DRYRUN=yes
    shift
    ;;
    -h|--help|help)
    print_help
    shift
    ;;
    -c|--create)
    CREATE_OR_UPDATE="create"
    shift
    ;;
    develop|staging|production|feature)
    DEPLOYMENT_ENV=$i
    shift
    ;;
	--skip-ecr)
	ECR_SKIP=yes
	shift
	;;
	-i=*|--image-suffix=*)
	IMAGE_SUFFIX="${i#*=}"
	shift
	;;
    -f=*|--feature-name=*)
    FEATURE_NAME="${i#*=}"
    shift
    ;;
    --region=*)
    REGION="${i#*=}"
    shift
    ;;
    *)
      error_exit $ERROR_INVALID_OPTIONS "${red}Unknown option ${bold}${i}${reset}${red} provided. Use --help for usage instructions.${reset}"
    ;;
esac
done

# ---- Validate Script Arguments ----
if [ -z "$DEPLOYMENT_ENV" ]; then
  echo "${red}${W} Missing Argument.${reset} Must provide environment name. E.g. ${bold}./build.sh staging${reset}"
  print_help $ERROR_INCOMPLETE
fi

if [ "$DEPLOYMENT_ENV" == "feature" ]; then
    if [ -z "$FEATURE_NAME" ]; then
      echo "${red}${W} Missing Argument.${reset} Must provide feature name. E.g. ${bold}./build.sh feature --feature-name=<featureName>${reset}"
      print_help $ERROR_INCOMPLETE
    fi
fi

if [ -z "IMAGE_SUFFIX" ]
then
	echo "IMAGE_SUFFIX not provided."
    IMAGE_SUFFIX=""
fi

# ---- Build Required Variables ----
if [ ! -f "./stack.yml" ]; then
    echo "${red}${W} stack.yml not found.${reset} There must be a stack.yml file in the root folder."
  error_exit $ERROR_INCOMPLETE 
fi

STACK_DESCRIPTION=`grep "^Description: '" ./stack.yml | awk '{print $2}' | sed -e 's|["'\'']||g'`
LOWERCASE_STACK_DESCRIPTION=`echo "$STACK_DESCRIPTION" | tr '[:upper:]' '[:lower:]'`
DEPLOYMENT_BUCKET="${LOWERCASE_STACK_DESCRIPTION}-deployment"

DEPLOYMENT_STACK_NAME=$DEPLOYMENT_ENV
if [ "$DEPLOYMENT_ENV" == "feature" ]; then
    DEPLOYMENT_STACK_NAME=$DEPLOYMENT_ENV-$FEATURE_NAME
fi

CONFIG_PATH="CloudFormation/$DEPLOYMENT_ENV"
CONFIG_URL="https://s3-${REGION}.amazonaws.com/${DEPLOYMENT_BUCKET}/${CONFIG_PATH}/"

LAMBDA_PATH="$DEPLOYMENT_ENV"
LAMBDA_BUCKET="${STACK_DESCRIPTION}-lambda-functions"

# ---- Script setup complete, build now! ----


# -----------------------------------------------------------------------------
echo "${bold}Generate ECS Task Definitions${reset}" 
# -----------------------------------------------------------------------------
for TEMPLATE in $(find . -name '*.yml.template')
do
  TARGET=${TEMPLATE%.*}
  echo -n "    Compiling $TEMPLATE to $TARGET"
  cfn-include $TEMPLATE -y > $TARGET
  
  if [[ $? = 0 ]];
  then 
    echo $OK
  else 
    echo "${red}${W} Failed to compile YAML${reset}"
    echo $VALIDATION_ERROR
    exit $ERROR_INVALID_TEMPLATE
  fi
done



# -----------------------------------------------------------------------------
echo "${bold}Validating templates${reset}"
# -----------------------------------------------------------------------------
for TEMPLATE in $(find . -name '*.yml')
do
  echo -n "    $TEMPLATE "
  VALIDATION_ERROR=$(aws cloudformation validate-template --template-body file://$TEMPLATE 2>&1 > /dev/null)
  
  if [[ $? = 0 ]];
  then 
    echo $OK
  else 
    echo "${red}${W} Invalid YAML${reset}"
    echo $VALIDATION_ERROR
    exit $ERROR_INVALID_TEMPLATE
  fi
done


if [ ! -z "$UPDATE_LAMBDAS" ] || [ $CREATE_OR_UPDATE == "create" ]
then
  # ---------------------------------------------------------------------------
  echo "${bold}Update lambda functions${reset}"
  # ---------------------------------------------------------------------------
  if [ ! -d "lambda" ] || [ ! "$(ls -A lambda)" ]; then
    echo -n "    No lambda functions found "
    echo $SKIPPED
  else
    echo -n "    Building packages "
    (cd ../lambdas && find . -maxdepth 1 -type d \( ! -name . \) -exec bash -c "cd '{}' && pwd && npm install && npm run zip" 1> /dev/null \;)
    echo $OK
  
    echo -n "    Uploading changed lambda functions "
    aws s3 sync ../lambdas/ s3://$LAMBDA_BUCKET/$LAMBDA_PATH/ --exclude "*" --include "*.zip" --quiet || error_exit $ERROR_S3
    echo $OK
  fi
fi

# -----------------------------------------------------------------------------
echo "${bold}Generate Lambda Function Mappings${reset}" 
# -----------------------------------------------------------------------------
if [ ! -d "lambda" ] || [ ! "$(ls -A lambda)" ]; then
  echo -n "    No lambda functions found "
  echo $SKIPPED
else
  echo -n "    Geting latest object versions "
  aws s3api list-object-versions --bucket $LAMBDA_BUCKET --prefix $LAMBDA_PATH/ | \
    jq --arg BUCKET $LAMBDA_BUCKET '.Versions | map(select(.IsLatest)) | map( {(.Key | capture("(?<l>[^/]+).zip") | .l): {"S3Bucket": $BUCKET, "S3Key": .Key, "S3ObjectVersion": .VersionId}} ) | add | {"LambdaSource": .}' | \
    json2yaml > resources/lambda/lambda-mappings.yml.include
  echo $OK

  echo -n "    Compiling lambda template with cfn-include "
  cfn-include ./resources/lambda/lambda-function.yml.template -y > ./resources/lambda/lambda-function.yml || error_exit $ERROR_INVALID_TEMPLATE
  echo $OK
fi

# -----------------------------------------------------------------------------
echo "${bold}Preparing Stack Parameters${reset}" 
# -----------------------------------------------------------------------------
BUILD_PARAMS_FILE=build-parameters.json
echo "    Creating JSON parameters file "

COMPILED_BUILD_PARAMS_FILE_TMP=$(mktemp /tmp/.cf-wrap-build-params.XXXXXXX)

if [ -f $BUILD_PARAMS_FILE ]; then
  jq --arg CONFIG_URL "$CONFIG_URL" --arg DEPLOYMENT_ENV "$DEPLOYMENT_ENV" --arg FEATURE_NAME "$FEATURE_NAME" --arg IMAGE_SUFFIX "$IMAGE_SUFFIX" \
    '. + [{"ParameterKey": "CloudFormationStackUrl", "ParameterValue": $CONFIG_URL}, {"ParameterKey": "DeploymentEnvironment", "ParameterValue": $DEPLOYMENT_ENV}, {"ParameterKey": "FeatureName", "ParameterValue": $FEATURE_NAME }, {"ParameterKey": "ImageSuffix", "ParameterValue": $IMAGE_SUFFIX }]' \
       < $BUILD_PARAMS_FILE > $COMPILED_BUILD_PARAMS_FILE_TMP || error_exit $ERROR_INVALID_JSON
else
  # No custom parameters
  jq --null-input --arg CONFIG_URL "$CONFIG_URL" --arg DEPLOYMENT_ENV "$DEPLOYMENT_ENV" --arg FEATURE_NAME "$FEATURE_NAME" --arg IMAGE_SUFFIX "$IMAGE_SUFFIX" \
    '. + [{"ParameterKey": "CloudFormationStackUrl", "ParameterValue": $CONFIG_URL}, {"ParameterKey": "DeploymentEnvironment", "ParameterValue": $DEPLOYMENT_ENV}, {"ParameterKey": "FeatureName", "ParameterValue": $FEATURE_NAME }, {"ParameterKey": "ImageSuffix", "ParameterValue": $IMAGE_SUFFIX }]' \
       > $COMPILED_BUILD_PARAMS_FILE_TMP || error_exit $ERROR_INVALID_JSON
fi
echo "                                  $OK"



BUILD_TAG="$DEPLOYMENT_ENV/`date +%Y/%m/%d-%H%M`"
STACK_NAME="${STACK_DESCRIPTION}-$(tr '[:lower:]' '[:upper:]' <<< ${DEPLOYMENT_STACK_NAME:0:1})${DEPLOYMENT_STACK_NAME:1}"

if [ ! -z "$DRYRUN" ] 
then
  echo
  echo "REGION         = $REGION"
  echo "STACK_NAME     = $STACK_NAME"
  echo "UPDATE_LAMBDAS = $UPDATE_LAMBDAS"
  echo "LAMBDA_PATH    = $LAMBDA_PATH"
  echo "BUILD_TAG      = $BUILD_TAG"
  echo "ECR_SKIP       = $ECR_SKIP"
  echo "Parameters:    `cat $COMPILED_BUILD_PARAMS_FILE_TMP`"
  echo
  echo "As --dryrun was specified, not making CloudFormation changes."
  rm $COMPILED_BUILD_PARAMS_FILE_TMP
  
  exit 0
fi

if [ $CREATE_OR_UPDATE == "create" ] && [ -f "./pre-build.yml" ] && [ $ECR_SKIP != yes ]; then
  # -----------------------------------------------------------------------------
  echo "${bold}Deploy pre-build stack${reset}"
  # -----------------------------------------------------------------------------
  echo -n "    checking for pre-build stack exports "
  ecrname=$(aws cloudformation list-exports | jq --raw-output '.Exports [] | select(.Name == "PreBuild-DeploymentBucket") | .Value')
  echo $OK
  

  echo -n "    aws cloudformation deploy --template-file ./pre-build.yml --stack-name PreBuild "
  if [ -z "$ecrname" ]; then
    aws cloudformation deploy --template-file ./pre-build.yml --stack-name PreBuild \
      --parameter-overrides CloudFormationStackUrl=$CONFIG_URL || error_exit
    echo $OK
    
    echo "Prebuild has been run, images and deployment keys can now be updated."
    exit
  else  
    echo $SKIPPED
  fi
fi


# -----------------------------------------------------------------------------
echo "${bold}Upload changed config files${reset}"
# -----------------------------------------------------------------------------
echo -n "    check access to deployment bucket $DEPLOYMENT_BUCKET "
aws s3 ls s3://$DEPLOYMENT_BUCKET > /dev/null || error_exit $ERROR_S3
echo $OK

echo -n "    aws s3 sync to s3://$DEPLOYMENT_BUCKET/$CONFIG_PATH/ "
aws s3 sync ./ s3://$DEPLOYMENT_BUCKET/$CONFIG_PATH/ --exclude "*" --include "*.yml" --quiet || error_exit $ERROR_S3
echo $OK


# -----------------------------------------------------------------------------
echo "${bold}Running $CREATE_OR_UPDATE-stack for $STACK_NAME in $REGION${reset}"
# -----------------------------------------------------------------------------
aws cloudformation $CREATE_OR_UPDATE-stack --region $REGION --stack-name $STACK_NAME \
  --template-url ${CONFIG_URL}stack.yml \
  --capabilities CAPABILITY_NAMED_IAM \
  --parameters file://$COMPILED_BUILD_PARAMS_FILE_TMP > /dev/null || error_exit $ERROR_AWS_CLI

rm $COMPILED_BUILD_PARAMS_FILE_TMP

start=`date +%s`
aws cloudformation wait stack-$CREATE_OR_UPDATE-complete --region $REGION --stack-name $STACK_NAME &
pid=$! # Process Id of the wait command

time_elapsed() {
  end=`date +%s`
  runtime_sec=$((end-start))
  
  if (( $runtime_sec > 59 )) ;
  then
    minutes=$(( $runtime_sec / 60 ))
    seconds=$(( $runtime_sec % 60 ))
    echo "${minutes}m ${seconds}s"
  else
    echo "${runtime_sec}s"
  fi
}

spin='-\|/'
i=0
while kill -0 $pid 2>/dev/null
do
  elapsed=`time_elapsed`
  i=$(( (i+1) %4 ))
  printf "\r    Waiting for stack to update ${yellow}${spin:$i:1} Updating (${elapsed})${reset}"
  sleep .1
done

elapsed=`time_elapsed`
printf "\r    Waiting for stack to update ${green}${O} Completed in ${elapsed}${reset}"

echo
echo
echo -n "Status of $STACK_NAME update: "
STACK_STATUS=`aws cloudformation describe-stack-events \
  --region $REGION  --stack-name $STACK_NAME --max-items 1 | jq '.StackEvents [0] .ResourceStatus'`

if [[ "$STACK_STATUS" = "\"UPDATE_ROLLBACK_COMPLETE\"" ]] || [[ "$STACK_STATUS" = "\"ROLLBACK_COMPLETE\"" ]];
then
  echo "${red}${W} Failed${reset} $STACK_STATUS"
else
  echo "${green}${O} Completed OK${reset}"  
  echo "${reset}${bold}Done.${reset}"
fi
    

