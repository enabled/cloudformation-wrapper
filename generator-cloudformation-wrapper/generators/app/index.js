const Generator = require('yeoman-generator');
const glob = require('glob');
const fs = require('fs')
const dirs = (p) => fs.readdirSync(p).filter(f => fs.statSync(p+"/"+f).isDirectory());


module.exports = class extends Generator {
  setSources() {
    this.sourceRoot(this.templatePath('../../../../base-layouts'));
  }
  
  setupNewProject() {
    return this.prompt([{
      type    : 'input',
      name    : 'name',
      message : 'Your project name',
      default : this.appname // Default to current folder name
    }, {
      type    : 'input',
      name    : 'shortName',
      message : 'The identifier to be used in all resources, usually an acronym or short word',
      default : this.appname.replace(' ', '_').toLowerCase().substring(0, 8) 
    }, {
      type    : 'list',
      name    : 'layout',
      message : 'Which layout would you like to use?',
      default : 'simple',
      choices : dirs(this.sourceRoot())
    }]).then((answers) => {
      this.log('Creating new stack:', answers.name);
      this.log('Stack template:', answers.layout);
      this._copy_stack(this.templatePath(answers.layout), answers.name, answers.shortName);
    });
  }

  _copy_stack(layout, projectName, shortName) {
    this.log(`_copy_stack(${layout}, ${projectName}, ${shortName})`);
    const templates = glob.sync(`${layout}/**/*.yml`).map(f => f.replace(`${this.sourceRoot()}/`, ''));
    templates.forEach(template => {
      this.fs.copyTpl(
        this.templatePath(template),
        this.destinationPath(`cloudformation/${template}`),
        { projectName: projectName, shortName: shortName }
      );
    })
  }
};