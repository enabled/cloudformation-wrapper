# Cross Account Access
## Introduction
It is possible to use one AWS user account to access many separate AWS Accounts (which may or 
may not be in the same _organisation_). To do this requires cross-account access.

## Setting Up Cross-Account Access

Consider there are two AWS accounts:

* The master account (where the user account you wish to use is)
* The target account (which you want to gain access to using the user from the master account)

The following are the instructions for doing this via the AWS console.

### 1. Set up role

1. Sign into the _target account_ console with a user that has IAM access
2. Go to **IAM**
3. Select _Roles_
4. Select _Create new role_
5. Select _Role for cross-account access_ and select **Provide access between AWS accounts you own**
6. Enter the Account ID for the _master account_ and then **Next Step**
7. Attach the appropriate permissions policy, eg _AdministratorAccess_
8. Name the role (e.g. Enabled) and click **Create Role**
9. Log out of the console

### 2. Set up permissions

1. Sign into the _master account_ console with a user that has IAM full access
2. Go to **IAM** and select _Groups_
3. Select _Create new group_ and enter a sensible group name ("MyOtherOrgAdministrators")
4. Select _Next_ and then _Next Step_ (don't select a policy), then _Create Group_
5. Select the newly created group, select the _Permissions_ tab, then create a new _Inline Policy_
6. Select _Policy Generator_

  Effect: _Allow_
  
  AWS Service: _AWS Security Token Service_
  
  Actions: _AssumeRole_
  
  Amazon Resource Name (ARN): _arn:aws:iam::<TargetAccountId>:role/<RoleName>_

7. Select _Add Statement_, then _Next Step_
8. Name the policy and click _Apply Policy_
9. Add the users to the group who should be able to use this

### 3. Switch to the Target Account in Console

Assuming that your user in the master account was one of the users added to the above group, you
can now switch to the target account in the AWS Console.

1. Select your account drop-down in the top navigation and select _Switch Role_
2. Add the account id (you can use the aliased name instead if you wish) and the role we created, plus add a display name
3. This will be remembered in you browser for future switching.

### 4. Switch accounts using the CLI

1. Open ~/.aws/config
2. Add a new profile for your target account, referencing your root account profile:

_e.g. (with added mfa serial if ya got it)_

  [profile ton]
  
  source_profile = enabled

  role_arn = arn:aws:iam::731903514957:role/EnabledAdministrator

  mfa_serial = arn:aws:iam::622453911828:mfa/jarrod.swift



Now using --profile with any AWS command will use that role.


## Setting Up Cross-Account Access using Organisations
In the case that the two accounts actually belong to a single company and you wish to make them
sub-accounts using AWS Organisations, a slightly different process is required.