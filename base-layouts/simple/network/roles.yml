Description: "<%= projectName %> - Network - IAM Policies and Roles"

Parameters:
  DeploymentEnvironment:
    Description: The deployment environment (staging|production|feature)
    Type: String


Resources:
  ecsInstanceRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Join ['-', ['ecsInstanceRole', !Ref DeploymentEnvironment]]
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Principal:
              Service:
                - "ec2.amazonaws.com"
            Action:
              - "sts:AssumeRole"

  ecsServiceRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Join ['-', ['ecsServiceRole', !Ref DeploymentEnvironment]]
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Principal:
              Service:
                - "ec2.amazonaws.com"
            Action:
              - "sts:AssumeRole"

  sesNotifierRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Join ['-', ['sesNotifier', !Ref DeploymentEnvironment]]
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Principal:
              Service:
                - "ec2.amazonaws.com"
            Action:
              - "sts:AssumeRole"

  snsNotifierRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Join ['-', ['snsNotifier', !Ref DeploymentEnvironment]]
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Principal:
              Service:
                - "ec2.amazonaws.com"
            Action:
              - "sts:AssumeRole"

  ecsInstancePolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: !Join ['-', ['ecsInstancePolicy', !Ref DeploymentEnvironment]]
      PolicyDocument:
        Version: 2012-10-17
        Statement:
         -
          Effect: Allow
          Resource: '*'
          Action:
           - ecs:DeregisterContainerInstance
           - ecs:DiscoverPollEndpoint
           - ecs:Poll
           - ecs:RegisterContainerInstance
           - ecs:StartTelemetrySession
           - ecs:Submit*
           - ecr:GetAuthorizationToken
           - ecr:BatchCheckLayerAvailability
           - ecr:GetDownloadUrlForLayer
           - ecr:BatchGetImage
           - logs:CreateLogStream
           - logs:PutLogEvents
      Roles:
       -
        !Ref ecsInstanceRole

  ecsServicePolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: !Join ['-', ['ecsServicePolicy', !Ref DeploymentEnvironment]]
      PolicyDocument:
        Version: 2012-10-17
        Statement:
         -
          Effect: Allow
          Resource: '*'
          Action:
           - elasticloadbalancing:Describe*
           - elasticloadbalancing:DeregisterInstancesFromLoadBalancer
           - elasticloadbalancing:RegisterInstancesWithLoadBalancer
           - ec2:Describe*
           - ec2:AuthorizeSecurityGroupIngress
      Roles:
       -
        !Ref ecsServiceRole

  s3DeploymentReadAllPolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: !Join ['-', ['s3DeploymentReadAllPolicy', !Ref DeploymentEnvironment]]
      PolicyDocument:
        Version: 2012-10-17
        Statement:
         -
          Effect: Allow
          Resource: 'arn:aws:s3:::tft-deployment/*'
          Action:
           - s3:Get*
      Roles:
       -
        !Ref ecsServiceRole
       -
        !Ref ecsInstanceRole

  sesPolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: !Join ['-', ['sesPolicy', !Ref DeploymentEnvironment]]
      PolicyDocument:
        Version: 2012-10-17
        Statement:
         -
          Effect: Allow
          Resource: '*'
          Action:
           - ses:*
      Roles:
       -
        !Ref sesNotifierRole

  snsPolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: !Join ['-', ['snsPolicy', !Ref DeploymentEnvironment]]
      PolicyDocument:
        Version: 2012-10-17
        Statement:
         -
          Effect: Allow
          Resource: '*'
          Action:
           - sns:*
      Roles:
       -
        !Ref snsNotifierRole

  ecsInstanceProfile:
    Type: "AWS::IAM::InstanceProfile"
    Properties:
      Roles:
        - !Ref ecsInstanceRole
      InstanceProfileName: !Join ['-', ['ecsInstanceRole', !Ref DeploymentEnvironment]]
