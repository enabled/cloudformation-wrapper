Description: "<%= projectName %> - Common - Cloudfront for a website"

Parameters:
  OriginWebDomain:
    Description: Origin website domain
    Type: String
  
  SiteCertificateArn: 
    Description: 'The ARN to the ACM certificate for the domain'
    Type: String

  SiteDomainZoneName: 
    Description: 'The top-level "hosted-zone" name for the site. The CDN URL will be <SiteSubDomain>.<SiteDomainZoneName>'
    Type: String
    
  SiteSubDomain:
    Description: 'The lowest level domain name for the site. The CDN URL will be <SiteSubDomain>.<SiteDomainZoneName>'
    Type: String
    Default: static

Resources:
  CloudFrontDistribution:
    Type: "AWS::CloudFront::Distribution"
    Properties:
      DistributionConfig:
        Comment: !Join [' ', ["CDN for website", !Ref OriginWebDomain]]
        Enabled: true
        DefaultRootObject: index.html
        Aliases:
          - !Join ['.', [!Ref SiteSubDomain, !Ref SiteDomainZoneName]]
        DefaultCacheBehavior:
          AllowedMethods: 
            - GET
            - HEAD
            - OPTIONS
          TargetOriginId: !Ref OriginWebDomain
          ViewerProtocolPolicy: redirect-to-https
          ForwardedValues:
            QueryString: false
        ViewerCertificate:
          AcmCertificateArn: !Ref SiteCertificateArn
          MinimumProtocolVersion: TLSv1
          SslSupportMethod: sni-only
        Origins:
          -
            DomainName: !Ref OriginWebDomain
            Id: !Ref OriginWebDomain
            CustomOriginConfig:
              HTTPSPort: 443
              OriginProtocolPolicy: https-only

  CloudFrontDNSRecordSet:
      Type: AWS::Route53::RecordSet
      Properties:
        HostedZoneName: !Join ['', [!Ref SiteDomainZoneName, '.']]
        Name: !Join ['', [!Ref SiteSubDomain, '.', !Ref SiteDomainZoneName, '.']]
        Type: CNAME
        TTL: 600
        ResourceRecords:
          - !GetAtt [CloudFrontDistribution, DomainName]


Outputs:
  StaticSiteUrl:
    Description: URL to the CDN
    Value: !Join ['', ['https://', !Ref SiteSubDomain, '.', !Ref SiteDomainZoneName]]