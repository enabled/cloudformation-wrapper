## Base Layouts

Demonstration stack templates for starting projects. These can be used be running the yeoman generator.

`npm install -g yo generator-cloudformation-wrapper`

`yo cloudformation-wrapper`


### Layouts

| Layout                 | Description                                                                                    |
| ---------------------- | ---------------------------------------------------------------------------------------------- |
| **[default](default)** | Multi-AZ layout with load balancer and ECS instances.                                          |
| **[simple](simple)**   | Multi-AZ layout with load balancer and ECS instances in only a few files (much faster).        |
