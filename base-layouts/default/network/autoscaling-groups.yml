Description: 'ReplaceMe - Network - AutoScaling Groups'

Parameters:
  DeploymentEnvironment:
    Description: The deployment environment (staging|production|feature)
    Type: String
  SubnetIds:
    Description: Subnets the loadbalancer will point to
    Type: String
  SecurityGroups:
    Description: Security groups the autoscaling launch config will have
    Type: String
  LoadBalancerName:
    Description: LoadBalancerName
    Type: String
  FeatureName:
    Description: 'Name of feature (DeploymentEnvironment must be "feature", feature-<feature_name>)'
    Type: String

Conditions:
  IsFeature: !Equals ['feature', !Ref DeploymentEnvironment]

Mappings:
  AZs:
    FamilyName:
      staging: 'ap-southeast-2a,ap-southeast-2b'
      production: 'ap-southeast-2a,ap-southeast-2b'
      feature: 'ap-southeast-2a,ap-southeast-2b'

  # Up to date ECS AMIs as of 07 May 2017
  # See http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI_launch_latest.html for latest
  ECSAMIRegionLookup:
    us-east-1:
        AMI: ami-0e297018
    us-east-2:
        AMI: ami-43d0f626
    us-west-1:
        AMI: ami-fcd7f59c
    us-west-2:
        AMI: ami-596d6520
    eu-west-1:
        AMI: ami-5ae4f83c
    eu-west-2:
        AMI: ami-ada6b1c9
    eu-central-1:
        AMI: ami-085e8a67
    ap-northeast-1:
        AMI: ami-f63f6f91
    ap-southeast-1:
        AMI: ami-b4ae1dd7
    ap-southeast-2:
        AMI: ami-ac5849cf
    ca-central-1:
        AMI: ami-8cfb44e8

Resources:
  
  LaunchConfiguration:
    Type: AWS::AutoScaling::LaunchConfiguration
    Properties:
      AssociatePublicIpAddress: true
      InstanceMonitoring: false
      IamInstanceProfile: !Join ['-', ['ecsInstanceRole', !Ref DeploymentEnvironment]] 
      EbsOptimized: false
      InstanceType: t2.micro
      ImageId: !FindInMap [ECSAMIRegionLookup, !Ref 'AWS::Region', AMI]
      # KeyName: !Join ['-', ['replaceme', !Ref DeploymentEnvironment]] # All feature environments use the same keypair
      SecurityGroups: !Split [',', !Ref SecurityGroups]
      UserData:
        Fn::Base64:
          Fn::Sub:
           - |
             #!/bin/bash

             yum install -y aws-cli || exit 1

             echo AWS_DEFAULT_REGION=${Region} >> /etc/ecs/ecs.config
             echo ECS_CLUSTER=${ECSCluster} >> /etc/ecs/ecs.config

           - {
             Region: !Ref 'AWS::Region',
             ECSCluster: !Join ['-', ['replaceme', !If [IsFeature, !Ref FeatureName, !Ref DeploymentEnvironment], 'services']]
             }

  AutoScalingGroup:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      Cooldown: 300
      DesiredCapacity: 1
      MinSize: 0
      MaxSize: 6
      AvailabilityZones: !Split [',', !FindInMap [AZs, FamilyName, !Ref DeploymentEnvironment]]
      VPCZoneIdentifier: !Split [',', !Ref SubnetIds]
      LaunchConfigurationName: !Ref LaunchConfiguration
      LoadBalancerNames:
        - !Ref LoadBalancerName
      Tags:
       -
        Key: Name
        Value: !Join ['-', ['replaceme', !If [IsFeature, !Ref FeatureName, !Ref DeploymentEnvironment], 'ASG']]
        PropagateAtLaunch: true

Outputs:
  LaunchConfiguration:
    Description: Launch config
    Value: !Ref LaunchConfiguration

  AutoScalingGroup:
    Description: The autoscaling group for replaceMe
    Value: !Ref AutoScalingGroup
