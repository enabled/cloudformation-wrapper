Description: 'ReplaceMe - Resources - RDS Instance'

Parameters:
  CloudFormationStackUrl:
    Description: The base url to get all stack configuration files from
    Type: String
     
  DeploymentEnvironment:
    Description: The deployment environment (staging|production|feature)
    Type: String

  PrivateSubnets:
    Description: The VPC subnets to put the DB in (multi AZ)
    Type: String

  FeatureName:
    Description: 'Name of feature (DeploymentEnvironment must be "feature", feature-<feature_name>)'
    Type: String

  RDSSecurityGroup:
    Description: Security group for RDS Server
    Type: String

Conditions:
  IsFeature: !Equals ['feature', !Ref DeploymentEnvironment]
  IsNotProduction: !Not [!Equals [!Ref DeploymentEnvironment, 'production']]

Resources:
  RDSSubnet:
    Type: AWS::RDS::DBSubnetGroup
    Properties:
      DBSubnetGroupDescription: DB Subnet for RDS Instance
      SubnetIds: !Split [',', !Ref PrivateSubnets]
      Tags:
       -
        Key: Name
        Value: !Join ['-', ['replaceme', !If [IsFeature, !Ref FeatureName, !Ref DeploymentEnvironment], 'RDS-subgroup']]

  RDSInstance:
    Type: AWS::RDS::DBInstance
    DeletionPolicy: 'Snapshot'
    Properties:
      AllocatedStorage: 100
      AllowMajorVersionUpgrade: false
      AutoMinorVersionUpgrade: true
      BackupRetentionPeriod: 2
      PreferredBackupWindow: '15:45-16:15'
      PreferredMaintenanceWindow: 'sun:11:00-sun:12:00'
      MultiAZ: !If [IsFeature, 'false', 'true']
      DBInstanceClass: db.t2.micro
      DBInstanceIdentifier: !Join ['-', ['replaceme', !If [IsFeature, !Ref FeatureName, !Ref DeploymentEnvironment], 'db']]
      DBSubnetGroupName: !Ref RDSSubnet
      DBSnapshotIdentifier: !If [IsNotProduction, 'replacemeedoc-snapshot', !Ref 'AWS::NoValue']
      Engine: mysql
      EngineVersion: 5.6.29
      VPCSecurityGroups:
       - !Ref RDSSecurityGroup
      Port: 3306
      StorageType: gp2 # general purpose SSD
      PubliclyAccessible: false
      StorageEncrypted: false
      MasterUsername: root
      MasterUserPassword: "replaceme_$MasDSSFrbd4546dGHf"
      Tags:
       -
        Key: Name
        Value: !Join ['-', ['replaceme', !If [IsFeature, !Ref FeatureName, !Ref DeploymentEnvironment], 'rds-instance']]

Outputs:
  RDSSubnet:
    Description: The Id of the RDS Subnet
    Value: !Ref RDSSubnet
  
  RDSHostAddress:
    Description: The database hostname
    Value: !GetAtt RDSInstance.Endpoint.Address  
    
  RDSHostPort:
    Description: The database hostname
    Value: !GetAtt RDSInstance.Endpoint.Port
