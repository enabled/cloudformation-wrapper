Description: 'ReplaceMe - Resources - s3 buckets'

Parameters:
  CloudFormationStackUrl:
    Description: The base url to get all stack configuration files from
    Type: String
     
  DeploymentEnvironment:
    Description: The deployment environment (staging|production|feature)
    Type: String

  SiteDomainZoneName:
    Description: The top-level "hosted-zone" name for the site.
    Type: String

  SiteCertificateArn:
    Description: The ARN to the ACM certificate for the domain
    Type: String

  FeatureName:
    Description: 'Name of feature (DeploymentEnvironment must be "feature", feature-<feature_name>)'
    Type: String

Conditions:
  IsNotFeature: !Not [!Equals [!Ref DeploymentEnvironment, 'feature']]

Resources:
  Public:
    Type: "AWS::CloudFormation::Stack"
    DeletionPolicy: Retain
    Condition: IsNotFeature
    Properties:
      TemplateURL: !Join ['', [!Ref CloudFormationStackUrl, 'common/cloudfront-s3.yml']]
      Parameters:
        S3BucketName: 'replaceme-public-${DeploymentEnvironment}-resources'
        SiteSubDomain: 'replaceme-static'
        SiteDomainZoneName: !Join ['.', [!Ref DeploymentEnvironment, !Ref SiteDomainZoneName]]
        SiteCertificateArn: !Ref SiteCertificateArn
        CreateS3Bucket: yes

  Private:
    Type: "AWS::CloudFormation::Stack"
    DeletionPolicy: Retain
    Condition: IsNotFeature
    Properties:
      TemplateURL: !Join ['', [!Ref CloudFormationStackUrl, 'common/cloudfront-signed-s3.yml']]
      Parameters:
        S3BucketName: !Sub "replaceme-private-${DeploymentEnvironment}"
        SiteSubDomain: 'replaceme-private'
        SiteDomainZoneName: !Join ['.', [!Ref DeploymentEnvironment, !Ref SiteDomainZoneName]]
        SiteCertificateArn: !Ref SiteCertificateArn
        CreateS3Bucket: yes

Outputs:
  Public:
    Description: Public bucket for replaceMe
    Value: !Ref Public

  Private:
    Description: Private bucket for replaceMe
    Value: !Ref Private
