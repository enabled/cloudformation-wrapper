#
# Create a Distribution for a non-public s3 bucket.
#
# Cloudformation cannot do all of this, the OriginAccessIdentity not only
# needs to be created outside of this process, but it requires the s3
# bucket to have a canonical user granted access!
#
#
#
Description: replaceMe - Common - Cloudfront for a private S3 bucket requiring pre-signed requests

Parameters:
  S3BucketName:
    Description: Name of the origin bucket
    Type: String
  
  SiteCertificateArn: 
    Description: 'The ARN to the ACM certificate for the domain'
    Type: String

  SiteDomainZoneName: 
    Description: 'The top-level "hosted-zone" name for the site. The CDN URL will be <SiteSubDomain>.<SiteDomainZoneName>'
    Type: String
    
  SiteSubDomain:
    Description: 'The lowest level domain name for the site. The CDN URL will be <SiteSubDomain>.<SiteDomainZoneName>'
    Type: String
    Default: static
    
  OriginAccessIdentity:
    Description: The Origin Access ID for cloudfront to use to connect to S3. This must be created manually
    Type: String
    Default: E1U311IADVB378
    
  CreateS3Bucket:
    Description: Set to yes if you need the s3 bucket to be created
    Type: String
    Default: no

Conditions:
  CreateBucket: !Equals [true, !Ref CreateS3Bucket]
  
Resources:
  DeploymentBucket:
    Type: AWS::S3::Bucket
    Condition: CreateBucket
    Properties:
      AccessControl: Private
      BucketName: !Ref S3BucketName
    DeletionPolicy: Retain
  
  CloudFrontDistribution:
    Type: "AWS::CloudFront::Distribution"
    Properties:
      DistributionConfig:
        Comment: !Join [' ', ["CDN for protected S3 bucket", !Ref S3BucketName]]
        Enabled: true
        DefaultRootObject: index.html
        Aliases:
          - !Join ['.', [!Ref SiteSubDomain, !Ref SiteDomainZoneName]]
        DefaultCacheBehavior:
          AllowedMethods: 
            - GET
            - HEAD
            - OPTIONS
          TargetOriginId: !Ref S3BucketName
          ViewerProtocolPolicy: redirect-to-https
          ForwardedValues:
            QueryString: false
        ViewerCertificate:
          AcmCertificateArn: !Ref SiteCertificateArn
          MinimumProtocolVersion: TLSv1
          SslSupportMethod: sni-only
        Origins:
          -
            DomainName: !Join ['.', [!Ref S3BucketName, 's3.amazonaws.com']]
            Id: !Ref S3BucketName
            S3OriginConfig:
              OriginAccessIdentity: !Join ['/', [origin-access-identity/cloudfront, !Ref OriginAccessIdentity]]

  CloudFrontDNSRecordSet:
      Type: AWS::Route53::RecordSet
      Properties:
        HostedZoneName: !Join ['', [!Ref SiteDomainZoneName, '.']]
        Name: !Join ['', [!Ref SiteSubDomain, '.', !Ref SiteDomainZoneName, '.']]
        Type: CNAME
        TTL: 600
        ResourceRecords:
          - !GetAtt [CloudFrontDistribution, DomainName]


Outputs:
  StaticSiteUrl:
    Description: URL to the signed CDN
    Value: !Join ['', ['https://', !Ref SiteSubDomain, '.', !Ref SiteDomainZoneName]]
