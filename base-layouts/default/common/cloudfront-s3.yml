Description: replaceMe - Common - Cloudfront for a S3 bucket

Parameters:
  S3BucketName:
    Description: Name of the origin bucket
    Type: String

  SiteCertificateArn: 
    Description: 'The ARN to the ACM certificate for the domain'
    Type: String

  SiteDomainZoneName: 
    Description: 'The top-level "hosted-zone" name for the site. The CDN URL will be <SiteSubDomain>.<SiteDomainZoneName>'
    Type: String

  SiteSubDomain:
    Description: 'The lowest level domain name for the site. The CDN URL will be <SiteSubDomain>.<SiteDomainZoneName>'
    Type: String
    Default: static

  CreateS3Bucket:
    Description: Set to yes if you need the s3 bucket to be created
    Type: String
    Default: no

Conditions:
  CreateBucket: !Equals [true, !Ref CreateS3Bucket]

Resources:
  SiteS3Bucket:
    Type: AWS::S3::Bucket
    Condition: CreateBucket
    Properties:
      AccessControl: PublicRead
      BucketName: !Ref S3BucketName
    DeletionPolicy: Retain
    
  SiteS3BucketPolicy:
    Type: "AWS::S3::BucketPolicy"
    Condition: CreateBucket
    Properties: 
      Bucket: !Ref SiteS3Bucket
      PolicyDocument: 
        Statement: 
          - 
            Action: 
              - "s3:GetObject"
            Effect: "Allow"
            Resource: !Join ["", ["arn:aws:s3:::", !Ref SiteS3Bucket, "/*"]]
            Principal: "*"
  
  CloudFrontDistribution:
    Type: "AWS::CloudFront::Distribution"
    Properties:
      DistributionConfig:
        Comment: !Join [' ', ["CDN for public S3 bucket", !Ref S3BucketName]]
        Enabled: true
        DefaultRootObject: index.html
        Aliases:
          - !Join ['.', [!Ref SiteSubDomain, !Ref SiteDomainZoneName]]
        DefaultCacheBehavior:
          AllowedMethods: 
            - GET
            - HEAD
            - OPTIONS
          TargetOriginId: !Ref S3BucketName
          ViewerProtocolPolicy: redirect-to-https
          ForwardedValues:
            QueryString: false
        ViewerCertificate:
          AcmCertificateArn: !Ref SiteCertificateArn
          MinimumProtocolVersion: TLSv1
          SslSupportMethod: sni-only
        Origins:
          -
            DomainName: !Join ['.', [!Ref S3BucketName, 's3.amazonaws.com']]
            Id: !Ref S3BucketName
            S3OriginConfig:
              OriginAccessIdentity: ""

  CloudFrontDNSRecordSet:
      Type: AWS::Route53::RecordSet
      Properties:
        HostedZoneName: !Join ['', [!Ref SiteDomainZoneName, '.']]
        Name: !Join ['', [!Ref SiteSubDomain, '.', !Ref SiteDomainZoneName, '.']]
        Type: CNAME
        TTL: 600
        ResourceRecords:
          - !GetAtt [CloudFrontDistribution, DomainName]


Outputs:
  StaticSiteUrl:
    Description: URL to the CDN
    Value: !Join ['', ['https://', !Ref SiteSubDomain, '.', !Ref SiteDomainZoneName]]
